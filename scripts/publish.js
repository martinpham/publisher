#!/usr/bin/env node

const {checkout, addAll, commit, prepareBuild, status, push} = require('../utils/git');
const parseArguments = require('minimist');
const {log, info, warn, error, dump, trim} = require('../utils/log');


const arguments = parseArguments(process.argv.slice(2));


const sourceBranch = arguments.source ? arguments.source : 'master';
const buildBranch = arguments.destination ? arguments.destination : 'dist';
const buildDirectory = arguments.build ? arguments.build : 'dist';
const whitelistDirectories = arguments.keep ? ((typeof arguments.keep === 'string' || typeof arguments.keep === 'number') ? [arguments.keep] : arguments.keep) : ['node_modules'];

(async () => {

    info('========== PREPARE ============');
    await prepareBuild(buildDirectory, whitelistDirectories);
    
    info('========== ADD all ============');
    await addAll();

    try {
        info('========== COMMIT build ============');
        await commit('build ' + (new Date()));
        info('========== PUSH build ============');
        await push(true, buildBranch);
    } catch (err) {
        error('========== COMMIT build error ============');
        console.log(err);
    }
        
    info('========== SWITCH MASTER ============');
    await checkout(sourceBranch);
    await status();
})();