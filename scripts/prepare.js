#!/usr/bin/env node

const {checkout, addAll, commit, deleteBranch} = require('../utils/git');
const parseArguments = require('minimist');
const {log, info, warn, error, dump, trim} = require('../utils/log');


const arguments = parseArguments(process.argv.slice(2));


const sourceBranch = arguments.source ? arguments.source : 'master';
const buildBranch = arguments.destination ? arguments.destination : 'dist';

(async () => {
    info('========== SWITCH master ============');
    await checkout(sourceBranch);
    try {
        info('========== ADD all ============');
        await addAll();

        info('========== COMMIT prebuild ============');
        await commit('prebuild');
    } catch(err) {
        error('========== COMMIT current jobs error ============');
        console.log(err);
    }
    
    
    info('========== SWITCH build ============');
    try {
        await deleteBranch(buildBranch);
    } catch(err) {
        error('========== DELETE build branch error ============');
        console.log(err);
    }
    await checkout(buildBranch, true);
})();
