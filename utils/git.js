const fs = require('fs');
const exec = require('./exec');

const getVersion = async () => {
    const {stdout} = await exec('git --version');

    return stdout;
}


const pull = async (branch = 'master', repository = 'origin') => {
    const {stdout} = await exec(`git pull ${repository} ${branch}`);

    return stdout;
}

const addAll = async () => {
    const {stdout} = await exec(`git add -A`);

    return stdout;
}

const commit = async (message) => {
    const {stdout} = await exec(`git commit -m '${message}'`);

    return stdout;
}


const status = async (message) => {
    const {stdout} = await exec(`git status`);

    return stdout;
}

const deleteBranch = async (branch = 'master', repository = 'origin') => {

    try {
        await exec(`git push --delete ${repository} ${branch}`);
    } catch (e) {

    }

    try {
        await exec(`git branch -D ${branch}`);    
    } catch (e) {
    
    }
}

const checkout = async (branch = 'master', createBranch = false) => {
    await exec(`git checkout ${createBranch ? '-b' : ''} ${branch}`);
}

const prepareBuild = async (buildDirectory = 'build', whitelistDirectories = []) => {
    let command = 'ls -pA | grep -v .git/ | grep -v .gitignore ';

    command += ` | grep -v ${buildDirectory}`;
    for(let directory of whitelistDirectories)
    {
        command += ` | grep -v ${directory}`;
    }

    command += ` | xargs rm -rf`;


    await exec(command);
    await exec(`cp -rv ${buildDirectory}/* .`);
    await exec(`rm -rf ${buildDirectory}`);
}


const push = async (force = true, branch = 'master', repository = 'origin') => {
    const {stdout} = await exec(`git push ${force ? '-f' : ''} ${repository} ${branch}`);

    return stdout;
}

module.exports = {
    getVersion,
    pull,
    addAll,
    commit,
    status,
    deleteBranch,
    checkout,
    prepareBuild,
    push
}


