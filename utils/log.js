const _log = (...placeholders) => {
	placeholders.unshift('[' + (new Date).toUTCString() + ']');

	return console.log.apply(this, placeholders);
};

const log = (...placeholders) => {
	placeholders.unshift('\x1b[32m');
	placeholders.push('\x1b[0m');

	return _log.apply(this, placeholders);
};
const error = (...placeholders) => {
	placeholders.unshift('\x1b[31m');
	placeholders.push('\x1b[0m');

	return _log.apply(this, placeholders);
};
const warn = (...placeholders) => {
	placeholders.unshift('\x1b[34m');
	placeholders.push('\x1b[0m');

	return _log.apply(this, placeholders);
};
const info = (...placeholders) => {
	placeholders.unshift('\x1b[33m');
	placeholders.push('\x1b[0m');

	return _log.apply(this, placeholders);
};
const trim = (variable) => {
	return variable.replace(/(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]/, '');
};
const dump = (variable) => {
	return trim(JSON.stringify(variable, null, 2));
};


module.exports = {log, info, warn, error, dump, trim};