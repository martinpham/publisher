const ChildProcess = require('child_process');
const {log, info, warn, error, dump, trim} = require('./log');

const exec = (command, options = {}) => {
    info('[exec] `', command, '` with options ', dump(options));
    return new Promise((resolver, rejector) => 
        ChildProcess.exec(command, options, (err, stdout, stderr) => 
            setTimeout(() => {
                if(err)
                {
                    error('[exec] ERROR ', trim(err + ''));
                    rejector(err);
                } else {
                    info('[exec] SUCCESS ', dump({ stdout, stderr }));
                    resolver({ stdout, stderr });
                }
            }, 100)
        )
    )
};

module.exports = exec;